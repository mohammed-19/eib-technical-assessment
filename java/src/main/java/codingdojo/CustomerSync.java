package codingdojo;

public class CustomerSync {

    private final CustomerDataAccess customerDataAccess;

    public CustomerSync(CustomerDataLayer customerDataLayer) {
        this(new CustomerDataAccess(customerDataLayer));
    }

    public CustomerSync(CustomerDataAccess db) {
        this.customerDataAccess = db;
    }

    public boolean syncWithDataLayer(ExternalCustomer externalCustomer) {

        CustomerMatches customerMatches = this.customerDataAccess.loadCustomer(externalCustomer);

        Customer customer = customerMatches.getCustomer();

        customer = initCustomer(externalCustomer, customer);

        boolean created = customer.getInternalId() == null;

        populateFields(externalCustomer, customer);

        saveCustomer(customer);

        if (customerMatches.hasDuplicates()) {
            for (Customer duplicate : customerMatches.getDuplicates()) {
                saveCustomer(initCustomer(externalCustomer, duplicate));
            }
        }

        setShoppingListAndStore(externalCustomer, customer);

        return created;
    }

    private Customer initCustomer(ExternalCustomer externalCustomer, Customer customer) {
        if (customer == null) {
            customer = new Customer();
            customer.setExternalId(externalCustomer.getExternalId());
            customer.setMasterExternalId(externalCustomer.getExternalId());
        }
        customer.setName(externalCustomer.getName());
        return customer;
    }

    private void setShoppingListAndStore(ExternalCustomer externalCustomer, Customer customer) {
        customer.setPreferredStore(externalCustomer.getPreferredStore());
        for (ShoppingList consumerShoppingList : externalCustomer.getShoppingLists()) {
            this.customerDataAccess.updateShoppingList(customer, consumerShoppingList);
        }
    }

    public void saveCustomer(Customer customer) {
        if (customer.getInternalId() == null) {
            this.customerDataAccess.createCustomerRecord(customer);
        } else {
            this.customerDataAccess.updateCustomerRecord(customer);
        }
    }

    private void populateFields(ExternalCustomer externalCustomer, Customer customer) {
        customer.setAddress(externalCustomer.getPostalAddress());
        if (externalCustomer.isCompany()) {
            customer.setCompanyNumber(externalCustomer.getCompanyNumber());
            customer.setCustomerType(CustomerType.COMPANY);
        } else {
            customer.setCustomerType(CustomerType.PERSON);
            customer.setBonusPointsBalance(externalCustomer.getBonusPointsBalance());
        }
    }

}
